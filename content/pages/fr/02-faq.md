Title: FAQ
Date: 2017-03-11 10:02
Slug: faq
Summary: FAQ
toc_run: true
toc_title: Foire aux questions
lang: fr

## Utilisation au quotidien

### Le solde d'un de mes comptes bancaires n'est pas correct dans Kresus. Que dois-je faire ?

Cela peut arriver parce que le logiciel utilisé par Kresus, Weboob, donne un
instantané des données bancaires présentes sur le site de votre banque, alors
que Kresus essaie de reconstruire un film entier. Si les données fournies par
votre banque ne sont pas cohérentes au cours du temps (une donnée change),
alors le solde d'un de vos comptes risque d'être incorrect dans Kresus.

Il est conseillé de regarder dans la section "Doublons" de Kresus. Si certaines
opérations semblent être des doublons, cette page vous les présentera et il
vous sera possible de les fusionner, à la main, pour retrouver le solde
correct. Cependant, la manière de faire est rudimentaire et certaines
opérations marquées comme doublons pourraient en fait ne pas en être. Il n'est
pas possible d'annuler la fusion de deux opérations, donc soyez prudents lors
de la suppression des doublons !

Dans le futur, il sera possible d'annuler une fusion de doublons. La détection
automatique de doublons devrait également être grandement améliorée.

### Comment puis-je ajouter manuellement une opération ?

Une fonctionnalité peu documentée (car encore très expérimentale) est de
pouvoir faire ses comptes à la main avec Kresus. Typiquement, il peut être
parfois utile de pouvoir renseigner des dépenses à l’avance, avant même
qu’elles n'apparaissent sur le site de notre banque et qu’elles ne soient
récupérées par Kresus.

Pour ajouter manuellement une opération, il suffit d’aller dans les
“Préférences” puis de cliquer sur le petit signe “+” à la fin de la ligne du
compte sur lequel on veut ajouter une opération.

![Bouton d'ajout d'opération](../images/pages/fr/ajout-op-bouton.png)

Lorsque la véritable opération apparaîtra sur votre compte bancaire et sera
récupérée par Kresus, il y aura un doublon créé, que vous devrez fusionner
manuellement. Vous aurez éventuellement à utiliser le bouton “En trouver plus”
si la date de l’opération manuelle est très différente de la date de
l’opération réelle.

![Fenêtre d'ajout d'opération](../images/pages/fr/ajout-op-modale.png)

## Sécurité

### Comment sont stockées mes coordonnées bancaires dans Kresus ?

Si vous utilisez la version autonome de Kresus, il est assumé que vous savez
protéger vos arrières, et notamment sécuriser un minimum votre serveur. Aucune
donnée n'est chiffrée dans ce mode de fonctionnement, pas même votre mot de
passe bancaire ; il vous est donc conseillé de chiffrer le disque sur lequel
est utilisé Kresus, ou encore de containeriser le service web, ou encore
d'utiliser d'autres subterfuges pour chiffrer le mot de passe. Dans le futur,
il est envisageable que le mot de passe soit chiffré dans ce mode de
fonctionnement également.

### Pourquoi dois-je donner mon mot de passe bancaire à Kresus ?

Pour récupérer vos opérations bancaires sur le site de votre banque, Kresus se
fait passer pour un navigateur web (par l'intermédiaire de Weboob), puis se
connecte au site de votre banque à l'aide de votre login et de votre mot de
passe. Cela permet de rapatrier vos opérations bancaires, de manière
automatique, toutes les nuits, sans aucune intervention nécessaire de votre
part.

### Pourquoi Kresus ne supporte pas l'authentification à deux facteurs ?

Comme Kresus se connecte à votre banque de la même façon que vous le feriez
avec votre navigateur, mais qu'il le fait chaque nuit, il faudrait qu'il vous
demande à chaque fois de saisir votre identifiant pour le second facteur, ce
qui pose bien évidemment problème : Kresus ne serait plus autonome.

## Au sujet de Kresus

### J'ai une question qui n'est pas dans cette Foire Aux Questions et qui devrait l'être, à mon avis.

Super ! N'hésitez-pas à venir nous la poser sur le
[forum](http://community.kresus.org/) ou à [ouvrir un
ticket](https://framagit.org/kresusapp/kresus.org/issues/new) sur le bug
tracker du site web directement. Si vous avez une ébauche de réponse, c'est
encore mieux ! Si vous vous la posez, il y a des chances que vous ne soyez pas
le/la seul⋅e !

### Comment rapporter un bug ?

Kresus est fait par des humains, et à ce titre peut contenir des erreurs ! Si
jamais vous rencontrez un problème lors de votre utilisation de Kresus,
n'hésitez-pas à [ouvrir un
ticket](https://framagit.org/kresusapp/kresus/issues/new) dans notre bug
tracker. Dans votre ticket, essayez d'inclure le plus d'informations possible
sur votre configuration : quelle version de Kresus utilisez-vous ? Quelle
version de Weboob utilisez-vous ? Quelles sont toutes les étapes nécessaires à
la reproduction du bug ? Plus nous aurons d'informations, plus les chances que
nous résolvions votre bug rapidement seront élevées.

### J'aimerais une nouvelle fonctionnalité.

Les idées ne manquent pas et sont toujours appréciées ! Vous pouvez également
[ouvrir un ticket](https://framagit.org/kresusapp/kresus.org/issues/new) pour
proposer une fonctionnalité, et par la suite voire même commencer à
l'implémenter ! Les mainteneurs seront ravis de vous aider à la mettre en
œuvre.

### Qui êtes-vous ?

Kresus étant un logiciel libre, nous sommes une [communauté de
développeurs](https://framagit.org/kresusapp/kresus/graphs/master) à l'avoir
créé. Nous ne sommes pas financés par CozyCloud ou Weboob et sommes des
développeurs indépendants, intéressés par le logiciel libre et les outils de
finances personnelles.
